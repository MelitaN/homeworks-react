import React, {useState} from "react";

function SelectColors(){

    const [color, setColor] = useState('');

    function SelectedColor(color){
        setColor(color);
    }

    return(
        <div>
            <button onClick={() => SelectedColor('Red')}>Red</button>
            <button onClick={() => SelectedColor('Blue')}>Blue</button>
            <button onClick={() => SelectedColor('Green')} >Green</button>
            <p>Selected Color: {color}</p>
        </div>
    )
}

export default SelectColors;