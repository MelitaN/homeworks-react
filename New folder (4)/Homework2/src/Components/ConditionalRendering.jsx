import React, {useState} from "react";

function ConditionalRendering(){
    const [value, setValue] = useState(true);

    return(
        <div>
            <p>{value? 'Welcome,User!' : ''}</p>
            <button onClick={() => setValue(!value)}>{value? 'Log out' : "Login"}</button>
        </div>
    )
}

export default ConditionalRendering;